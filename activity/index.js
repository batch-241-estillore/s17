/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
	let promptMessage = "What is your name?"
	function promptUser(promptMessage){
		let message = prompt(promptMessage);
		return message;
	}

	let name = promptUser(promptMessage);
	console.log("Hello, "+name);
	promptMessage = "How old are you?"
	let age = promptUser(promptMessage);
	console.log("You are "+age+" years old.")
	promptMessage = "Where do you live?";
	let address = promptUser(promptMessage);
	alert("Thank you for your input!");
	console.log("You live in "+address);



/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
	function favoriteBands(){
		let bands = ['Bread','Pan!c','The Eagles','The Beatles','Jacob Collier'];
		console.log('My favorite bands/artists are:')
		console.log(bands);
	}
	favoriteBands();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	//third function here:
	function favoriteMovies(){
		console.log('My favorite movies are:')
		console.log("1. Knives Out");
		console.log("Rotten Tomatoes Rating: 97%");
		console.log("2. Schindler's list");
		console.log("Rotten Tomatoes Rating: 98%");
		console.log("3. Mystic River");
		console.log("Rotten Tomatoes Rating: 88%");
		console.log("4. V for Vendetta");
		console.log("Rotten Tomatoes Rating: 73%");
		console.log("5. Moulin Rouge!");
		console.log("Rotten Tomatoes Rating:75%");
	}
	favoriteMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};
printFriends();

/*console.log(friend1);
console.log(friend2);*/