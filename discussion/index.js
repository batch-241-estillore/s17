/*alert("Hello!");*/


/*Functions
	Funcions in javascript are lines/block of codes that tell our device/application to perform a certain task when called/invoked

	Functions are mostly created to create complicated tasks to run several lines of code in succession

*/

//Function Declaration
	// (function statement) defines a function with the specified parameters

/*
	syntax:
		function functionName(){
			codeblock(statement);
		}
	function keyword - used to define a javascript function
	function block - statements which comprise the body of the function. This is also where the code to be executed

*/	

function printName(){
	console.log("My name is John");
}

/*
	The code block and statements inside a function is not immediately executed when the function is defined. The code block and statements inside a function is executed when the function is invoked or called.
*/
printName();

// undeclaredFunction(); does not work


//hoisting	
declaredFunc();

//Function Declaration vs. Fuction Expression
function declaredFunc(){
	console.log("Hello world from the declaredFunc()");
}
declaredFunc();

//Function Expressions
	/*
		A function can also be stored in a variable. This is called a function expression

		Function expression is an anonymous function assigned to the variableFunction
	*/
//variableFunc(); Uncaught ReferenceError: Cannot access 'variableFunc' before initialization

let variableFunc = function() {
	console.log("Hello from the variableFunc");
}

variableFunc();

let funcExpression = function funcName(){
	console.log("Hello from the other side!");
};

funcExpression();

//Can we reassign declared functions and function expressions to a NEW anonymous function?

declaredFunc = function(){
	console.log("Updated declaredFunc");
}
declaredFunc();

funcExpression = function(){
	console.log("Updated funcExpression");
}
funcExpression();

const constantFunc = function(){
	console.log("Initialized with CONST");
}
constantFunc();

//	Uncaught TypeError: Assignment to constant variable:
	/*constantFunc = function(){
		console.log("Can we reassign?");
	}*/

//Function Scoping
	/*
		Scope is the accessibility of our variables

		JS Variables has 3 types of scope:
			1.Local/block scope
			2.Global Scope
			3.Function Scope
	*/
{
	let localVar = 'I am a local variable';
}
let globalVar = 'I am a global variable';
console.log(globalVar);


function showNames(){
	//Function scoped variables
	var functionVar = "Joe"
	const functionConst = "John"
	let functionLet = "Jane"

	console.log(functionVar);
	console.log(functionConst);
	console.log(functionLet);
}

showNames();
/*console.log(functionVar);
console.log(functionConst);
console.log(functionLet);*/ //Uncaught ReferenceError: functionVar is not defined

//nested function
	function myNewFunction() {
		let name = "Maria";
		function nestedFunction(){
			let nestedName = "Jose";
			console.log(name);
		}
		nestedFunction();
	}

	myNewFunction();
	//nestedFunction(); this will cause an error

//Function and Global Scope variables

	//Global Variable
	let globalName = "Josh";
	function myNewFunction2(){
		let nameInside = "Jenno";
		console.log(globalName);

	}
	myNewFunction2();
	// console.log(nameInside);

//Using Alert()
/*
	Syntax:
		alert(message);
 */
// 	alert("Hello World!");

	function showSampleAlert(){
		alert("Hello User!");
	}
	// showSampleAlert();

	console.log("Hello after alert!");

//Using Prompt()

	// let samplePrompt = prompt("Enter your name:");
	// console.log(typeof samplePrompt) //Prompt returns string data type if okayed and null if cancelled
	// console.log("Hello, " + samplePrompt);

	function printWelcom(){
		let firstName = prompt("Enter your First Name:")
		let lastName = prompt("Enter your Last Name:")

		console.log("Hello, "+firstName+' '+lastName+ '!');
		console.log("Welcome to my page!")

	}
	printWelcom();



